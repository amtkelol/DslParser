﻿module Dsl.SemanticAnalysis

open System.Collections.Generic
open Dsl.Ast
open Dsl.AstUtils
open Dsl.CompilerException.CompilerErrors

type SymbolTable = Dictionary<Identifier, PacketField>

let buildSymbolTable p =
    let table = new SymbolTable()
    let identifierFromField f =
        match f with
        | Arr a ->
            a.id
        | Var v ->
            v.id
        | Enumeration e ->
            e.id
        | PacketRef p ->
            p.id

    let processElement element =
        let field = element.field
        let id = identifierFromField field
        if table.ContainsKey id then
            field |> getFieldName |> fieldAlreadyDefined |> raise
        table.Add(id, field)

    p.elements
        |> Seq.iter processElement
    table

let reservedKeywords =
    let cartesian xs ys =
        xs |> List.collect (fun x -> ys |> List.map (fun y -> x + y))

    let reserved = ["packet"; "end_of_stream"]

    let dataTypes = [
        "uint8"; "int8";
        "uint16"; "int16";
        "uint32"; "int32";
        "uint64"; "int64";
        "float4"; "float8"]

    let endian = [ "_le"; "_be"]
    let pairs = cartesian dataTypes endian
    reserved @ dataTypes @ pairs

let findReservedKeyWords (table: SymbolTable) =
    let fieldNames =
        table.Keys
        |> Seq.map getIdentifierName

    let head = Set.intersect (Set.ofSeq fieldNames) (Set.ofSeq reservedKeywords) |> Seq.tryHead
    match head with
    | Some field ->
        reservedKeyword field |> raise
    | None ->
        ()

let checkByteOrder p =
    let raiseByteOrder order field =
        match order with
        | Some _ ->
            ()
        | None ->
            getFieldName field |> byteOrderNotSet |> raise

    let processElements element =
        match element.field with
        | Var v ->
           raiseByteOrder v.byteOrder element.field
        | Arr a ->
           raiseByteOrder a.byteOrder element.field
        | Enumeration _ ->
            ()
        | PacketRef _ ->
            ()

    match p.options with
    | Some _ ->
       ()
    | None ->
        p.elements |> Seq.iter processElements

let checkArrayId (table: SymbolTable) =
    let eos = Identifier "end_of_stream"
    let processField =
        function
        | Arr a ->
            match a.size with
            | Id i  ->
                if not (table.ContainsKey i) && i <> eos then
                    getIdentifierName i |> doesNotExist |> raise
            | _ ->
                ()
        | _ ->
            ()

    table.Values |> Seq.iter processField

// Ugly. Consider refactoring
let checkElementAttrs elements =
    let calculate acc =
        function
        | Contents _ ->
            acc + 1

    let processElementOption s =
        s
        |> Seq.fold (fun acc elem ->
            let result = calculate acc elem
            match result with
            | a when a > 1 ->
                elem.ToString() |> attributeError |> raise
            | _ ->
                result
            ) 0
        |> ignore

    elements
        |> Seq.choose (fun f ->
            if not <| Seq.isEmpty f.attrs
                then Some f.attrs
                else None
        )
        |> Seq.iter processElementOption

let analyze p =
    let root = p.root
    let table = buildSymbolTable root

    checkByteOrder root
    findReservedKeyWords table
    checkArrayId table
    checkElementAttrs root.elements
