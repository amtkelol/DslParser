﻿module Dsl.Progam

open System.IO
open Dsl

let inline readLines file = File.ReadAllText(file)

let inline resultFileName file = Path.GetFileNameWithoutExtension(file) |> sprintf "%s.hpp"

let parse source result =
    let input = readLines source
    let ast = Parser.parse input
    SemanticAnalysis.analyze ast
    CppCodeGenerator.generate ast result

[<EntryPoint>]
let main argv =
    let arglist = argv |> List.ofSeq
    match arglist with
    | source :: tail ->
        try
            let result = resultFileName source
            parse source result
        with ex ->
            printfn "%A" ex.Message
    | _ ->
        printfn "Usage: DslParser.exe source_file.dsl"
    0
