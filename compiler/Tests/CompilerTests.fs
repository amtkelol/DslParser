﻿module Tests.CompilerTests

open FsUnit
open NUnit.Framework
open Dsl
open Dsl.CompilerException

[<Test>]
let ``should raise fieldAlreadyDefined``()=
    let source = "
    [byte_order : little_endian]
    packet foo = {
        uint16 packet_number
        uint8 resource_number
        uint16 src_port
        uint16 src_port
    }"

    let ast = Parser.parse source
    (fun () ->
        SemanticAnalysis.analyze ast |> ignore ) |> should (throwWithMessage "DSL002 A field named 'src_port' is already defined") typeof<CompilerException>

[<Test>]
let ``should raise reservedKeyword``()=
    let source = "packet foo = {
        uint16_le packet_number
        uint16_be uint16
    }"

    let ast = Parser.parse source
    (fun () ->
        SemanticAnalysis.analyze ast |> ignore ) |> should (throwWithMessage "DSL003 'uint16' is a reserved keyword") typeof<CompilerException>

[<Test>]
let ``should raise byteOrderNotSet``()=
    let source = "packet foo = {
        uint16 packet_number
        int32 foo
    }"

    let ast = Parser.parse source
    (fun () ->
        SemanticAnalysis.analyze ast |> ignore ) |> should (throwWithMessage "DSL004 byte order for field 'packet_number' is not set") typeof<CompilerException>

[<Test>]
let ``should not raise byteOrderNotSet``()=
    let source = "packet foo = {
        uint16_le packet_number
        int32_be foo
    }"

    let ast = Parser.parse source
    (fun () ->
        SemanticAnalysis.analyze ast |> ignore ) |> should not' (throw typeof<CompilerException>)

[<Test>]
let ``should raise doesNotExist``()=
    let source = "
        [byte_order : little_endian]
        packet foo = {
            uint8 size
            int8 array[qwerty]
    }"

    let ast = Parser.parse source
    (fun () ->
        SemanticAnalysis.analyze ast |> ignore ) |> should (throwWithMessage "DSL005 A field named 'qwerty' does not exist") typeof<CompilerException>

[<Test>]
let ``should raise attributeError``()=
    let source = "
    [byte_order : little_endian]
    packet foo = {
        uint16 packet_number
        uint8 resource_number
        uint16 src_port
        [contents : {1, 2}; contents : {3, 4}]
        uint16 dst_port[2]
    }"

    let ast = Parser.parse source
    (fun () ->
        SemanticAnalysis.analyze ast |> ignore ) |> should (throwWithMessage "DSL006 An attribute 'Contents [3; 4]' is already defined") typeof<CompilerException>

[<Test>]
let ``should raise nothing``()=
    let source = "
    [byte_order : little_endian]
    packet foo = {
        uint16 packet_number
        uint8 resource_number
        uint16 src_port
        [contents : {1,2,3}]
        uint16 dst_port[3]
    }"

    let ast = Parser.parse source
    (fun () ->
        SemanticAnalysis.analyze ast |> ignore ) |> should not' (throw typeof<CompilerException>)
