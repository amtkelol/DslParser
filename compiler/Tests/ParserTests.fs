﻿module Tests.ParserTests

open FParsec
open FsUnit
open NUnit.Framework
open Dsl
open Dsl.Ast
open Dsl.Parser

type InitMsgUtils() =
    inherit FSharpCustomMessageFormatter()

let runParser parser input =
    match run parser input with
    | Success(result, _, _) ->
        result
    | Failure(msg, _, _) ->
        failwith msg

[<Test>]
let testFramework()=
    2 + 2 |> should equal 4

[<Test>]
let ``should parse id`` ()=
    runParser parseId "ab" |> should equal (Identifier "ab")

[<Test>]
let ``should parse field`` ()=
    let expected = Var {dataType = UInt16; id = Identifier("name"); byteOrder = None}
    runParser parseField "uint16 name" |> should equal expected

[<Test>]
let ``should parse packet`` ()=
    let source =
        "packet foo = {
            uint16 header
            int8 array[128]
            uint8 version
        }"
    let packet = Parser.parse source
    let expected = {
        options = None;
        name = "foo";
        elements =
        [
            {attrs = List.empty; field = Var {dataType = UInt16; id = Identifier "header"; byteOrder = None}};
            {attrs = List.empty; field = Arr {dataType = Int8; id = Identifier "array"; size = Literal(128); byteOrder = None}};
            {attrs = List.empty; field = Var {dataType = UInt8; id = Identifier "version"; byteOrder = None}}
        ]
    }

    packet |> should equal { root = expected; refs = List.empty }

[<Test>]
let ``should parse array`` ()=
    runParser parseField "int8 foo[256]" |> should equal (Arr { dataType = Int8; id = Identifier("foo"); size = Literal(256); byteOrder = None })

// NUnit TestCaseSource does not work with .NET Core for some reason
#if !NETCOREAPP2_0
let dataTypeSource =
    [
    "uint8", UInt8, None
    "uint16", UInt16, None
    "uint32", UInt32, None
    "uint64", UInt64, None
    "int8", Int8, None
    "int16", Int16, None
    "int32", Int32, None
    "int64", Int64, None
    "float4", Float4, None
    "float8", Float8, None
    "int8_le", Int8, Some(LittleEndian)
    "int32_be", Int32, Some(BigEndian)
    "float8_le", Float8, Some(LittleEndian)
    ] |> Seq.map (fun (a, b, c) -> TestCaseData(a, b, c))

[<TestCaseSource("dataTypeSource")>]
let ``should parse data type`` typeString (expectedType: DataType) (byteOrder: option<Endianness>) =
    runParser parseDataType typeString |> should equal (expectedType, byteOrder)

#endif

[<Test>]
let ``should parse array with space``() =
    let field = "int8 array [32]"
    runParser parseField field |> should equal (Arr { dataType = Int8; id = Identifier("array"); size = Literal(32); byteOrder = None})

[<Test>]
let ``should parse array with byte order``() =
    let field = "uint32_be array[128]"
    runParser parseField field |> should equal (Arr { dataType = UInt32; id = Identifier("array"); size = Literal(128); byteOrder = Some(BigEndian)})

[<Test>]
let ``should parse packet with endiannes option``()=
    let source = "
        [byte_order : little_endian]
        packet foo = {
            uint16 header
            uint8_be version
        }"

    let packet = Parser.parse source
    let expected = {
        options = Some(ByteOrder(LittleEndian));
        name = "foo";
        elements =
        [
            {attrs = List.empty; field = Var {dataType = UInt16; id = Identifier "header"; byteOrder = None}};
            {attrs = List.empty; field = Var {dataType = UInt8; id = Identifier "version"; byteOrder = Some BigEndian}}
        ]
    }

    packet |> should equal { root = expected; refs = List.empty }

[<Test>]
let ``should parse array with identifier size``()=
    let source = "
    [byte_order : little_endian]
    packet foo = {
        uint8 size
        int8 array[size]
    }"

    let packet = Parser.parse source
    let expected = {
        options = Some(ByteOrder(LittleEndian));
        name = "foo";
        elements =
        [
            {attrs = List.empty; field = Var {dataType = UInt8; id = Identifier "size"; byteOrder = None}};
            {attrs = List.empty; field = Arr {dataType = Int8; id = Identifier "array"; size = Id (Identifier "size"); byteOrder = None}}
        ]
    }

    packet |> should equal { root = expected; refs = List.empty }

[<Test>]
let ``should parse packet element`` ()=
    let source = "[contents : {1, 2, 3, 4}]
    uint8 magic[4]"

    let result = runParser Parser.parsePacketElement source
    let expected = { attrs = [Contents [1; 2; 3; 4;]]; field = Arr {dataType = UInt8; id = Identifier "magic"; size = Literal 4; byteOrder = None} }

    result |> should equal expected

[<Test>]
let ``should parse multiple options``()=
    // TODO: We are not allowed to define same option twice
    let attr = "[contents : {1, 2, 3, 4}; contents: {0xFF, 0xBE, 42, 0x10}]"
    let result = runParser Parser.parseElementOption attr
    let expected = [Contents [1; 2; 3; 4]; Contents [255; 190; 42; 16]]
    result |> should equal expected

[<Test>]
let ``should parse packet with no options``()=
    let source = "
        packet foo = {
            uint8_le size
        }"

    let packet = Parser.parse source
    let expected = {
        options = None;
        name = "foo";
        elements = [{attrs = List.empty; field = Var {dataType = UInt8; id = Identifier "size"; byteOrder = Some LittleEndian}};]
    }

    packet |> should equal { root = expected; refs = List.empty }

[<Test>]
let ``should parse enum``()=
    let source = "enum ip_proto = {
        icmp: 1
        tcp: 6
        udp: 17
    }"

    let expected = Enumeration {
        id = Identifier "ip_proto";
        pairs =
        [
            {name = "icmp"; value = 1};
            {name = "tcp"; value = 6};
            {name = "udp"; value = 17};
        ]
    }

    runParser parseEnum source |> should equal expected

[<Test>]
let ``should parse packet with enum``()=
    let source =
        "packet foo = {
            uint8_le size
            enum ip_proto = {
                icmp: 1
                tcp: 6
                udp: 17
            }
        }"

    let packet = Parser.parse source
    let expected = {
        options = None;
        name = "foo";
        elements =
        [
            {attrs = List.empty; field = Var {dataType = UInt8; id = Identifier "size"; byteOrder = Some LittleEndian}};
            {attrs = List.empty; field = Enumeration {
                id = Identifier "ip_proto";
                pairs =
                [
                    {name = "icmp"; value = 1};
                    {name = "tcp"; value = 6};
                    {name = "udp"; value = 17};
                ]
            }}
        ]
    }

    packet |> should equal { root = expected; refs = List.empty }

[<Test>]
let ``should parse packet reference``()=
    let source =
        "packet foo = {
            uint8_le size
            dns_proto bar
        }"
    let packet = Parser.parse source
    let expected = {
        options = None;
        name = "foo";
        elements =
        [
            {attrs = List.empty; field = Var {dataType = UInt8; id = Identifier "size"; byteOrder = Some LittleEndian}};
            {attrs = List.empty; field = PacketRef { id = Identifier "bar"; refId = Identifier "dns_proto"; packetRef = ref { options = None; name = ""; elements = Seq.empty } }};

        ]
    }

    packet |> should equal { root = expected; refs = List.empty }

[<Test>]
let ``should parse multiple packets with ref``()=
    let source =
        "packet foo = {
            uint8_le size
            dns_proto bar
        }
        packet dns_proto = {
            uint8_le version
        }"

    let packet = runParser Parser.parseRootPacket source
    let root = {
        options = None;
        name = "foo";
        elements =
            [{attrs = List.empty; field = Var {dataType = UInt8; id = Identifier "size"; byteOrder = Some LittleEndian}};
            {attrs = List.empty; field = PacketRef { id = Identifier "bar"; refId = Identifier "dns_proto"; packetRef = ref { options = None; name = ""; elements = Seq.empty } }};
        ]
    }

    let refs = [
        {
        options = None;
        name = "dns_proto";
        elements =
            [{attrs = List.empty; field = Var {dataType = UInt8; id = Identifier "version"; byteOrder = Some LittleEndian}}]

        }]

    let expected = { root = root; refs = refs }
    packet |> should equal expected
