﻿module Dsl.CppCodeGenerator

open System.IO
open System.Text
open Dsl.Ast
open Dsl.AstUtils
open Dsl.Writer
open Dsl.CompilerException.CompilerErrors

let writeGuardStart (packetName: string) writer =
    let uppercaseName = packetName.ToUpper() + "_H"
    writer
        >>~ ("#ifndef %s", uppercaseName)
        >>~ ("#define %s", uppercaseName)
        |> newLine

let writeGuardEnd (packetName: string) writer =
    let uppercaseName = packetName.ToUpper() + "_H"
    writer >>~ ("#endif // !%s", uppercaseName)

let writeInclude writer =
    writer
     >>+ "#include <Dsl/DslMemoryStream.h>"
     >>+ "#include <Dsl/DslStream.h>"
     |> newLine
     >>+ "#include <cstdint>"
     |> newLine

let writeClassBegin name writer =
    writer >>~ ("class %s_t {", name)

let writeClassEnd writer =
    writer >>+ "};"

let writeConstructor name writer =
    writer
        >>+ "public:"
        |> increaseIndent
        >>~ ("%s_t(dslcompiler::DslStream *io) {", name)
        |> increaseIndent
        >>+ "_io = io;"
        >>+ "read();"
        |> decreaseIndent
        >>+ "}"
        |> resetIndentLevel
        |> newLine

let writeDestructor name writer =
    writer
        |> increaseIndent
        >>~ ("~%s_t() {", name)
        >>+ "}"
        |> decreaseIndent
        |> newLine

let getCppType =
    function
    | Int8 -> "int8_t"
    | UInt8 -> "uint8_t"
    | Int16 -> "int16_t"
    | UInt16 -> "uint16_t"
    | UInt32 -> "uint32_t"
    | Int32 -> "int32_t"
    | UInt64 -> "uint64_t"
    | Int64 -> "int64_t"
    | Float4 -> "float"
    | Float8 -> "double"

let getIdPublicName id=
    getIdentifierName id |> sprintf "%s"

let getIdPrivateName id =
    getIdentifierName id |> sprintf "_%s"

let dslVarToField v =
    (getCppType v.dataType, getIdPrivateName v.id) ||> sprintf "%s %s;"

let dslArrayToField id =
    ("std::string", getIdPrivateName id) ||> sprintf "%s %s;"

let dslEnumToField (e: DslEnum) =
    let sb = new StringBuilder()
    let writer = Writer.fromStringBuilder(sb)
    let enumName = getIdentifierName e.id

    writer
        >>~ ("enum %s_t {", enumName)
        |> increaseIndent
        |> increaseIndent
        |> ignore

    e.pairs
        |> Seq.map (fun pair -> sprintf "%s = %s," pair.name (pair.value.ToString()))
        |> Seq.iter writer.writeLine

    writer
        |> decreaseIndent
        >>+ "};"
        |> ignore

    writer
        |> newLine
        >>+ sprintf "%s_t _%s;" enumName enumName
        |> ignore

    sb.ToString()

let fieldToString f =
    match f with
    | Var v ->
        v |> dslVarToField
    | Arr a ->
        a.id |> dslArrayToField
    | Enumeration e ->
        dslEnumToField e
    | _ ->
        f.ToString() |> sprintf "unable to convert field %s" |> codeGeneratorError |> raise

let writeDslFields writer =
    writer >>+ "dslcompiler::DslStream *_io;"

let writeFields fields writer =
    writer
        >>+ "private:"
        |> increaseIndent
        |> ignore

    fields
        |> Seq.map fieldToString
        |> Seq.iter writer.writeLine

    writer
        |> writeDslFields
        |> resetIndentLevel
        |> newLine

let varToGetterString v =
    (getCppType v.dataType, getIdPublicName v.id, getIdPrivateName v.id) |||> sprintf "%s %s() const { return %s; }"

let arrToGetterString (a: DslArray) =
    ("std::string", getIdPublicName a.id, getIdPrivateName a.id) |||> sprintf "%s %s() const { return %s; }"

let enumToGetterString (e: DslEnum) =
    let pubName = getIdPublicName e.id
    (pubName, pubName, getIdPrivateName e.id) |||> sprintf "%s_t %s() const { return %s; }"

let writeGetters fields writer =
    writer
        >>+ "public:"
        |> increaseIndent
        |> ignore

    let fieldToGetter f =
        match f with
        | Var v ->
            v |> varToGetterString
        | Arr a ->
            a |> arrToGetterString
        | Enumeration e ->
            e |> enumToGetterString
        | _ ->
            f.ToString() |> sprintf "unable to convert field %s to getter" |> codeGeneratorError |> raise

    fields
        |> Seq.map fieldToGetter
        |> Seq.iter writer.writeLine

    writer
        |> resetIndentLevel
        |> newLine

let variableToReadFunc var defaultByteOrder =
    let typeToReadFunc =
        function
        | Int8 -> "_io->read_s1"
        | UInt8 -> "_io->read_u1"
        | Int16 -> "_io->read_s2"
        | UInt16 -> "_io->read_u2"
        | Int32 -> "_io->read_s4"
        | UInt32 -> "_io->read_u4"
        | Int64 -> "_io->read_s8"
        | UInt64 -> "_io->read_u8"
        | Float4 -> "_io->read_f4"
        | Float8 -> "_io->read_f8"

    let orderToString =
        function
        | LittleEndian -> "le"
        | BigEndian -> "be"

    let order =
        match var.byteOrder with
        // If variable have byte order property
        | Some bo ->
            bo
        // Use default byte order otherwise
        | None ->
            match defaultByteOrder with
            | Some dbo ->
                dbo
            // This case shouldn't happen. See SemanticAnalysis.checkByteOrder
            | None ->
                var.ToString() |> sprintf "unable to convert variable %s to read function" |> codeGeneratorError |> raise

    match var.dataType with
    | Int8 | UInt8 ->
        sprintf "%s()" (typeToReadFunc var.dataType)
    | _ ->
        sprintf "%s%s()" (typeToReadFunc var.dataType) (orderToString order)

let enumToReadString (e: DslEnum) =
    sprintf "%s = static_cast<%s_t>(_io->read_u1());" <|| (getIdPrivateName e.id, getIdentifierName e.id)

let elementToReadString packetByteOrder element =
    let defaultByteOrder =
        match packetByteOrder with
        | Some e ->
            let (ByteOrder bo) = e
            Some bo
        | None ->
            None

    let varToString (v: Variable) =
         sprintf "_%s = %s;" (getIdentifierName v.id) <| variableToReadFunc v defaultByteOrder

    let hasContentsAttribute elem =
        elem.attrs |> Seq.tryPick (fun o ->
            match o with
            | Contents c ->
                Some c
        )

    let contentsToReadString (a: DslArray) (cont: int seq) =
        let contents = cont |> Seq.fold (fun acc x -> acc + "\x" + x.ToString("X")) String.Empty
        sprintf "_%s =_io->ensure_fixed_contents(std::string(\"%s\"));" (getIdentifierName a.id) contents

    // TODO: Refactor
    let arrayToReadString arr =
        let sizeToString =
            match arr.size with
            | Literal l ->
                l.ToString()
            | Id id ->
                sprintf "%s()" <| getIdentifierName id

        match arr.size with
        | Id id when id = Identifier "end_of_stream" ->
            sprintf "_%s = _io->read_bytes_full();" <| getIdentifierName arr.id
        | _ ->
            sprintf "_%s = _io->read_bytes(%s);" (getIdentifierName arr.id) <| sizeToString

    match element.field with
    | Var v ->
        varToString v
    | Arr a ->
        match hasContentsAttribute element with
        | Some c ->
            contentsToReadString a c
        | None ->
            arrayToReadString a
    | Enumeration e ->
        enumToReadString e
    | _ ->
        element.field.ToString() |> sprintf "unable to convert field %s to read function" |> codeGeneratorError |> raise

let writeReadFunc packet writer =
    writer
        >>+ "public:"
        |> increaseIndent
        >>+ "void read() {"
        |> increaseIndent
        |> ignore

    packet.elements
        |> Seq.map (elementToReadString packet.options)
        |> Seq.iter writer.writeLine

    writer
        |> decreaseIndent
        >>+ "}"
        |> resetIndentLevel
        |> newLine

let processPacket writer p =
    let fields = p.elements |> Seq.map (fun e -> e.field)
    writer
        |> writeGuardStart p.name
        |> writeInclude
        |> writeClassBegin p.name
        |> writeConstructor p.name
        |> writeDestructor p.name
        |> writeFields fields
        |> writeReadFunc p
        |> writeGetters fields
        |> writeClassEnd
        |> writeGuardEnd p.name
        |> ignore

let generate rootPacket resultFile =
    let sb = new StringBuilder()
    let writer = Writer.fromStringBuilder(sb)

    let p = rootPacket.root
    processPacket writer p

    use file = File.CreateText(resultFile)
    sb.ToString() |> file.WriteLine
