﻿module Dsl.Parser

open FParsec
open Dsl.Ast
open Dsl.CompilerException.CompilerErrors

let ws = spaces
let ws1 = spaces1
let pcharWs c = pchar c .>> ws

let parseDataType =
    let int8 = stringReturn "int8" Int8
    let int16 = stringReturn "int16" Int16
    let int32 = stringReturn "int32" Int32
    let int64 = stringReturn "int64" Int64

    let uint8 = stringReturn "uint8" UInt8
    let uint16 = stringReturn "uint16" UInt16
    let uint32 = stringReturn "uint32" UInt32
    let uint64 = stringReturn "uint64" UInt64

    let float4 = stringReturn "float4" Float4
    let float8 = stringReturn "float8" Float8

    let dataType = choiceL [int8; int16; int32; int64; uint8; uint16; uint32; uint64; float4; float8] "data type"
    let byteOrder = stringReturn "le" LittleEndian <|> stringReturn "be" BigEndian
    dataType .>>. opt (pchar '_' >>. byteOrder)

let isAsciiIdStart c = isAsciiLetter c
let isAsciiIdNext c = isAsciiLetter c || isDigit c || c = '_'
let identOpts = IdentifierOptions(isAsciiIdStart = isAsciiIdStart, isAsciiIdContinue = isAsciiIdNext)
let idWs = identifier identOpts .>> ws

let parseArraySize =
    let literal = pint32 |>> Literal
    let id = identifier identOpts |>> fun name -> Id(Identifier(name))

    literal <|> id

let parseId =
    identifier identOpts |>> Identifier <?> "identifier"

let parseArray =
    let convertToArray (((dType, bo), id), size) =
        Arr { dataType = dType; id = id; size = size; byteOrder = bo }

    parseDataType .>> ws1 .>>. parseId .>> ws .>> pchar('[') .>>. parseArraySize .>> pchar(']') <?> "array" |>> convertToArray

let parseVar =
    let convertToVar ((dType, bo), id) =
        Var {dataType = dType; id = id; byteOrder = bo}

    parseDataType .>> ws1 .>>. parseId <?> "variable" |>> convertToVar

let parseEnum =
    let parseEnumPair =
        idWs .>> pcharWs(':') .>>. pint32 <?> "key : value enum pair"
            |>> fun (id, value) -> { name = id; value = value }

    // TODO: Which one is better? Also, check whitespace separator
    let pairs = between (pcharWs '{') (ws >>. pchar '}') (many1 (parseEnumPair .>> ws))
    //let pairs2 = between (pcharWs '{') (pcharWs '}') (sepBy parseEnumPair ws1)

    pstring "enum" >>. ws1 >>. parseId .>> ws .>> pcharWs '=' .>>. pairs <?> "enum defenition"
        |>> fun (id, pairs) -> Enumeration { id = id; pairs = pairs }

let parsePacketRef =
    let dummyRef = { options = None; name = String.Empty; elements = Seq.empty }

    parseId .>> ws1 .>>. parseId <?> "packet reference"
        |>> fun (refId, id) -> PacketRef { refId = refId; id = id; packetRef = ref dummyRef }

let parseField =
    attempt parseArray <|> parseVar <|> parseEnum <|> parsePacketRef

let parseByteOrder =
    let little = stringReturn "little_endian" LittleEndian
    let big = stringReturn "big_endian" BigEndian
    let endianness = little <|> big

    pstring "byte_order" >>. ws >>. pcharWs ':' >>. endianness |>> ByteOrder

let parsePacketOption =
    between (pstring "[") (pstring "]") parseByteOrder <?> "packet options"

let parseContents =
    let hexToInt c = (int c &&& 15) + (int c >>> 6) * 9
    let hexInt32 = pstring "0x" >>. hex |>> hexToInt
    let numbers = pint32 <|> hexInt32
    let array = between (pchar '{') (pchar '}') (sepBy numbers (pcharWs ','))

    pstring "contents" >>. ws >>. pcharWs ':' >>. array |>> fun c -> Contents(c)

let parseElementOption =
    let options = sepBy1 parseContents (pcharWs ';') <?> "element options"

    between (pstring "[") (pstring "]") options

let parsePacketElement =
    let packetAttribute = opt (ws >>. parseElementOption .>> ws1) |>> (function
        | Some attrs ->
            attrs
        | None ->
            List.empty
    )
    let variable = ws >>. parseField

    let convertToElement (a, f) =
        {attrs = a; field = f}

    packetAttribute .>>. variable |>> convertToElement

let parsePacket =
    let packetOptions = ws >>. parsePacketOption .>> ws1
    let packetKeyword = ws >>. pstring "packet" <?> "packet keyword"
    let packetName = identifier identOpts <?> "packet name"

    let packetElements = between (pchar '{') (pchar '}') (many1 (ws >>. parsePacketElement .>> ws))

    opt packetOptions .>> packetKeyword .>> ws1 .>>. packetName .>> ws .>> pcharWs '=' .>>. packetElements <?> "packet definition"

let parseRootPacket =
    let convertToDslPacket ((option, pname), pelements) =
        {options = option; name = pname; elements = pelements}

    let convertToRoot (root, refs) =
        {root = root; refs = refs}

    let parseSinglePacket = ws >>. parsePacket .>> ws |>> convertToDslPacket
    parseSinglePacket .>>. many parseSinglePacket .>> eof |>> convertToRoot

let parse input =
    match run parseRootPacket input with
    | Success(result, _, _) -> result
    | Failure(msg, _, _) -> raise (parserError msg)
