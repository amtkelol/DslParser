﻿module Dsl.AstUtils

open Dsl.Ast

let inline getIdentifierName (Identifier name) = name

let getFieldName field =
    match field with
    | Var v ->
        getIdentifierName v.id
    | Arr a ->
        getIdentifierName a.id
    | Enumeration e ->
        getIdentifierName e.id
    | PacketRef r ->
        getIdentifierName r.id
