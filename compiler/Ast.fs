﻿module Dsl.Ast

type Endianness =
    | LittleEndian
    | BigEndian

type PacketOption =
    ByteOrder of Endianness

type ElementOption =
    Contents of int seq

type DataType = 
    | Int8
    | Int16
    | Int32
    | Int64
    | UInt8
    | UInt16
    | UInt32
    | UInt64
    | Float4
    | Float8

type Identifier =
    Identifier of string

type ArraySize =
    | Id of Identifier
    | Literal of int

type DslArray = {
    dataType: DataType
    id : Identifier
    size: ArraySize
    byteOrder: option<Endianness>
}

type Variable = {
    dataType: DataType
    id: Identifier
    byteOrder: option<Endianness>
}

type EnumPair = {
    value: int
    name: string
}

type DslEnum = {
    id : Identifier
    pairs : EnumPair seq
}

type PacketField =
    | Var of Variable
    | Arr of DslArray
    | Enumeration of DslEnum
    | PacketRef of PacketReference

and PacketReference = {
    refId: Identifier
    id: Identifier
    packetRef: DslPacket ref
}

and DslPacket = {
    options: option<PacketOption>
    name: string
    elements: PacketElement seq
}

and PacketElement = {
    attrs: ElementOption seq
    field: PacketField
}

type DslRootPacket = {
    root: DslPacket
    refs: DslPacket seq
}
